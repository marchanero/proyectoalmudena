%Creacion del listado de los elementos de un directorio
clear 
clc

listing = dir('/Users/robertosanchezreolid/Repositorios/proyectoalmudena/ficheros_mat/csvFile');

path_in='/Users/robertosanchezreolid/Repositorios/proyectoalmudena/ficheros_mat/csvFile/';
path_out='/Users/robertosanchezreolid/Repositorios/proyectoalmudena/ficheros_mat/MatFiles/';


%Extraccion de los datos de la estructura 
listing(3).name

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Comienzo del iterador
for c = 3:130
    %%%%%%%%%% Cambos en el string para el nombre
    nombre_completo=listing(c).name;
    nombre=split(listing(c).name,'.');
 
    %Hay que usar la funcion para poder obtenero los datos y luego guardarlo en
    %el otro directorio.

    eeg_data=eeg_csv_mat(strcat(path_in,nombre_completo),[2,Inf]);

    %% Convert to output type
    eeg_data = table2array(eeg_data);

    salida=strcat(path_out,nombre(1),'.mat');

    salida1=char(salida(1,1))
    save(salida1,'eeg_data')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end




clear 

 