clear
close all
clc

load('alpha_memories.mat','d');


no_memories = d(1:49, 1:end-1);
no_memories_media = mean(no_memories);
memories = d(50:65, 1:end-1);
memories_media = mean(memories);

maximo = max(max(no_memories_media),max(memories_media));
minimo = min(min(no_memories_media),min(memories_media));

dispen_diff_media =  memories_media - no_memories_media;
maxdif = max(dispen_diff_media);
mindif = min(dispen_diff_media);


figure(1)
subplot(1,3,1)
[handle,Zi,grid,Xi,Yi] = topoplot2(no_memories_media,'emotiv1.ced',...
    'electrodes','on', 'maplimits', [minimo-0.04, maximo-0.04] )
title('NO MEMORY')
subplot(1,3,2)
[handle,Zi,grid,Xi,Yi] = topoplot2(memories_media,'emotiv1.ced',...
    'electrodes','on', 'maplimits', [minimo-0.04, maximo-0.04])
title('MEMORY')
subplot(1,3,3)
[handle,Zi,grid,Xi,Yi] = topoplot(dispen_diff_media,'emotiv1.ced',...
    'electrodes','on', 'maplimits', [mindif-0.00075, maxdif-0.00075])
title('DIFFERENCE')

