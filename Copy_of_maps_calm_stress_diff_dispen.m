clear
close all
clc

%% Mapas entropias calculadas

load('classification_matrices.mat','DISPEN');


%% QSampEn m = 2 r = 0.25
dispen_calm = DISPEN(1:137, 1:end-1);
dispen_calm_media = mean(dispen_calm);
dispen_stress = DISPEN(138:259, 1:end-1);
dispen_stress_media = mean(dispen_stress);

maximo = max(max(dispen_calm_media),max(dispen_stress_media));
minimo = min(min(dispen_calm_media),min(dispen_stress_media));

dispen_diff_media = dispen_calm_media - dispen_stress_media;
maxdif = max(dispen_diff_media);
mindif = min(dispen_diff_media);

figure(1)
subplot(1,3,1)
[handle,Zi,grid,Xi,Yi] = topoplot(dispen_calm_media,'channel32_DEAP.locs',...
    'electrodes','off', 'maplimits', [minimo-0.04, maximo-0.04])
title('CALM')
subplot(1,3,2)
[handle,Zi,grid,Xi,Yi] = topoplot(dispen_stress_media,'channel32_DEAP.locs',...
    'electrodes','off', 'maplimits', [minimo-0.04, maximo-0.04])
title('STRESS')
subplot(1,3,3)
[handle,Zi,grid,Xi,Yi] = topoplot(dispen_diff_media,'channel32_DEAP.locs',...
    'electrodes','off', 'maplimits', [mindif-0.00075, maxdif-0.00075])
title('DIFFERENCE')

